import {
  createAsyncThunk,
  createDraftSafeSelector,
  createSlice,
} from "@reduxjs/toolkit";
import api from "../../api";
import { RootState } from "../store";
import { loginType, registerType } from "../types/auth";
export const register = createAsyncThunk(
  "register",
  async (data: registerType, thunk) => {
    try {
      const res = await api.post("/user/register", data);
      localStorage.setItem("token", res.data._id);
      window.location.href = "/";
      return res.data;
    } catch (error) {
      return error;
    }
  }
);
export const login = createAsyncThunk(
  "login",
  async (data: loginType, thunk) => {
    try {
      const res = await api.post("/user/login", data);
      localStorage.setItem("token", res.data._id);
      window.location.href = "/";
      return res.data;
    } catch (error) {
      return error;
    }
  }
);
export const logout = createAsyncThunk("logout", async (data: any, thunk) => {
  try {
    const res = await api.post("/user/logout", data);
    localStorage.setItem("token", "");
    // window.history.pushState(null, "", "/");
    return res.data;
  } catch (error) {
    return error;
  }
});
export const getUser = createAsyncThunk("getUser", async (data: any, thunk) => {
  try {
    const res = await api.post("/user/me", data);
    return res.data;
  } catch (error) {
    return error;
  }
});
const slice = createSlice({
  name: "auth",
  initialState: {
    loader: false,
    _id: localStorage.getItem("token"),
    email: "",
    name: "",
    age: 0,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(
      register.fulfilled,
      ({ email, _id, age, name }, { payload }) => {
        email = payload.email;
        _id = payload._id;
        age = payload.age;
        name = payload.name;
      }
    );
    builder.addCase(
      login.fulfilled,
      ({ email, _id, age, name }, { payload }) => {
        email = payload.email;
        _id = payload._id;
        age = payload.age;
        name = payload.name;
      }
    );
    builder.addCase(
      getUser.fulfilled,
      ({ email, _id, age, name }, { payload }) => {
        email = payload.email;
        _id = payload._id;
        age = payload.age;
        name = payload.name;
      }
    );
    builder.addCase(
      logout.fulfilled,
      ({ email, _id, age, name }, { payload }) => {
        email = "";
        _id = "";
        age = 0;
        name = "";
      }
    );
  },
});
export const emailSelector = createDraftSafeSelector(
  (state: RootState) => state.auth,
  (auth) => auth.email
);
export const _idSelector = createDraftSafeSelector(
  (state: RootState) => state.auth,
  (auth) => auth._id
);
export const nameSelector = createDraftSafeSelector(
  (state: RootState) => state.auth,
  (auth) => auth.name
);
export const ageSelector = createDraftSafeSelector(
  (state: RootState) => state.auth,
  (auth) => auth.age
);
export default slice.reducer;
