import {
  createAsyncThunk,
  createDraftSafeSelector,
  createSlice,
} from "@reduxjs/toolkit";
import axios from "axios";
import api from "../../api";
import { RootState } from "../store";
import { loginType, registerType } from "../types/auth";
const addTask = createAsyncThunk(
  "addTask",
  async (data: registerType, thunk) => {
    try {
      const res = await api.post("/task", data);
      return res.data;
    } catch (error) {
      return error;
    }
  }
);
const getTasks = createAsyncThunk(
  "getTasks",
  async (data: loginType, thunk) => {
    try {
      const res = await api.get("/task");
      return res.data;
    } catch (error) {
      return error;
    }
  }
);
const getCompletedTask = createAsyncThunk(
  "getCompletedTask",
  async (data: any, thunk) => {
    try {
      const res = await api.get("/task?completed=true");
      return res.data;
    } catch (error) {
      return error;
    }
  }
);
const updateTask = createAsyncThunk("updateTask", async (data: any, thunk) => {
  try {
    const res = await api.put(`/task/${data.id}`, data);
    return res.data;
  } catch (error) {
    return error;
  }
});
const deleteTask = createAsyncThunk("deleteTask", async (data: any, thunk) => {
  try {
    const res = await api.post(`/task/${data.id}`, data);
    return res.data;
  } catch (error) {
    return error;
  }
});
const slice = createSlice({
  name: "auth",
  initialState: {
    loader: false,
    token: localStorage.getItem("token"),
    email: "",
    name: "",
    password: "",
    age: 0,
  },
  reducers: {},
  extraReducers: (builder) => {},
});
export const emailSelector = createDraftSafeSelector(
  (state: RootState) => state.auth,
  (auth) => auth.email
);
export const nameSelector = createDraftSafeSelector(
  (state: RootState) => state.auth,
  (auth) => auth.name
);
export const ageSelector = createDraftSafeSelector(
  (state: RootState) => state.auth,
  (auth) => auth.age
);
export default slice.reducer;
