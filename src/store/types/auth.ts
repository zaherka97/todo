export type registerType = {
  name: string;
  email: string;
  password: string;
  age: number;
};
export type loginType = {
  email: string;
  password: string;
};
