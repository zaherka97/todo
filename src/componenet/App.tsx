import React from "react";
import { ThemeProvider } from "@mui/material";
import Header from "./ui/Header";
import theme from "./ui/Theme";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Route, Router, Routes } from "react-router-dom";
import Login from "./login";
import Register from "./register";
import Profile from "./profile";
import Main from "./main";
import { Provider } from "react-redux";
import store from "../store/store";

function App() {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <Header />
          <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/" element={<Main />} />
          </Routes>
        </BrowserRouter>
      </ThemeProvider>
    </Provider>
  );
}
export default App;
