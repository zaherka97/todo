import { TextField } from "@mui/material";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { register } from "../../store/slicers/Auth";
import { useAppDispatch } from "../../store/store";

const Register = () => {
  //***************************************************************** */
  const [Register, setRegister] = useState({
    name: "",
    email: "",
    password: "",
    age: 0,
  });
  const dispatch = useAppDispatch();
  //***************************************************************** */
  //***************************************************************** */
  return (
    <div className="container">
      <p className="h3 mt-3 text-primary text-center">Register</p>
      <div className="text-center">
        <TextField
          id="input-with-icon-textfield"
          label="Name"
          variant="standard"
          size="medium"
          className="mt-5 w-50"
          onChange={(e) => setRegister({ ...Register, name: e.target.value })}
        />
        <br />
        <TextField
          id="input-with-icon-textfield"
          label="email"
          variant="standard"
          size="medium"
          className="mt-3 w-50"
          onChange={(e) => setRegister({ ...Register, email: e.target.value })}
        />
        <br />
        <TextField
          id="input-with-icon-textfield"
          label="password"
          variant="standard"
          size="medium"
          className="mt-3 w-50"
          onChange={(e) =>
            setRegister({ ...Register, password: e.target.value })
          }
        />
        <br />
        <TextField
          id="input-with-icon-textfield"
          label="age"
          variant="standard"
          size="medium"
          className="mt-3 w-50"
          onChange={(e) => setRegister({ ...Register, age: +e.target.value })}
        />
        <br />
        <button
          className="btn btn-primary mt-4 w-50"
          onClick={() => dispatch(register(Register))}
        >
          Register
        </button>
        <br />
        <Link className="btn btn-warning text-white mt-4 w-50" to={"/login"}>
          Already have account?Login
        </Link>
      </div>
    </div>
  );
};

export default Register;
