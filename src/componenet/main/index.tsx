import React from "react";
import ItemList from "../item";
import Modal from "../modal";

const Main = () => {
  return (
    <div className="container">
      <div className="row mt-4 bg-dark rounded p-5 text-white">
        <div className="col-6 ">
          <p className="display-4">Welcome Back,</p>
          <p className="display-4">Mostafa</p>
        </div>
        <div className="col-6">
          <p className="display-4 text-end">rain,33C</p>
        </div>
      </div>
      <div className="d-flex justify-content-around mt-4">
        <select
          className="form-select w-50 "
          aria-label="Default select example"
        >
          <option value="1">All</option>
          <option value="2">Completed</option>
          <option value="3">Not Completed</option>
        </select>
        <button
          className="btn btn-success"
          data-toggle="modal"
          data-target="#exampleModal"
        >
          Add new Task
        </button>
        <Modal />
      </div>
      <ItemList />
    </div>
  );
};

export default Main;
