import { TextField } from "@mui/material";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { login } from "../../store/slicers/Auth";
import { useAppDispatch } from "../../store/store";

const Login = () => {
  //***************************************************************** */
  const [Login, setLogin] = useState({
    email: "",
    password: "",
  });
  const dispatch = useAppDispatch();
  //***************************************************************** */
  return (
    <div className="container">
      <p className="h3 mt-3 text-primary text-center">Login</p>
      <div className="text-center">
        <TextField
          id="input-with-icon-textfield"
          label="email"
          variant="standard"
          size="medium"
          className="mt-5 w-50"
          onChange={(e) => setLogin({ ...Login, email: e.target.value })}
        />
        <br />
        <TextField
          id="input-with-icon-textfield"
          label="password"
          variant="standard"
          size="medium"
          className="mt-3 w-50"
          onChange={(e) => setLogin({ ...Login, password: e.target.value })}
        />
        <br />
        <button
          className="btn btn-primary mt-4 w-50"
          onClick={() => dispatch(login(Login))}
        >
          Login
        </button>
        <br />
        <Link className="btn btn-warning text-white mt-4 w-50" to={"/register"}>
          You dont have account?register
        </Link>
      </div>
    </div>
  );
};

export default Login;
