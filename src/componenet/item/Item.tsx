import React from "react";

const Item = () => {
  return (
    <div
      className="w-75 d-flex justify-content-between p-3 mx-auto rounded"
      style={{ backgroundColor: "#504f4b" }}
    >
      <span className="text-white">Item</span>
      <div className="">
        <span className="text-primary">edit</span>
        <span className="ms-4  text-primary">delete</span>
      </div>
    </div>
  );
};

export default Item;
