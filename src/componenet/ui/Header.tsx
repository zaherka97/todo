import {
  AppBar,
  Tab,
  Tabs,
  Toolbar,
  Typography,
  useScrollTrigger,
} from "@mui/material";
import React from "react";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { logout, _idSelector } from "../../store/slicers/Auth";
import { useAppDispatch, useAppSelector } from "../../store/store";

interface Props {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window;
  children: React.ReactElement;
}

function ElevationScroll(props: Props) {
  const { children, window } = props;
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window ? window() : undefined,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
}
interface LinkTabProps {
  label?: string;
  href?: string;
}
function LinkTab(props: LinkTabProps) {
  return (
    <Tab
      component="a"
      onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        event.preventDefault();
      }}
      {...props}
    />
  );
}

const Header = () => {
  const [value, setValue] = React.useState(0);

  //****************************************************************************** */
  const HandleChange = (event: any, newValue: number) => {
    setValue(newValue);
  };
  const token = useAppSelector(_idSelector);
  const dispatch = useAppDispatch();
  //****************************************************************************** */
  //****************************************************************************** */
  return (
    <>
      <ElevationScroll>
        <AppBar position="fixed" color="primary">
          <Toolbar>
            <Typography variant="h5">ToDo App</Typography>
            <Tabs
              value={value}
              onChange={HandleChange}
              aria-label="basic tabs example"
              textColor="secondary"
              indicatorColor="secondary"
              className="w-75 px-5"
            >
              {!token ? (
                <>
                  <Link
                    to={"/login"}
                    className="text-white"
                    onClick={() => setValue(0)}
                  >
                    <Tab label="Login" href="/login" />
                  </Link>
                  <Link
                    to={"/register"}
                    className="text-white"
                    onClick={() => setValue(1)}
                  >
                    <Tab label="Register" href="/register" />
                  </Link>
                </>
              ) : (
                <Link
                  to={"/login"}
                  className="text-white"
                  onClick={() => dispatch(logout({}))}
                >
                  <Tab label="Logout" href="/login" />
                </Link>
              )}
            </Tabs>
          </Toolbar>
        </AppBar>
      </ElevationScroll>
      <div style={{ minHeight: "63px" }} />
    </>
  );
};

export default Header;
