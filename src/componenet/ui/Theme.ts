import { CommonColors, createTheme } from "@mui/material";
const arcBlue = "#0B72B9";
const arcWhite = "#ffffff";

interface CustomCommon extends CommonColors {
  blue: string;
  orange: string;
}

const theme = createTheme({
  palette: {
    common: <CustomCommon>{
      blue: arcBlue,
      orange: arcWhite,
    },
    primary: {
      main: arcBlue,
    },
    secondary: {
      main: arcWhite,
    },
  },
});
export default theme;
